import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import argparse

def save_ecg_leads_csv(df, p_uuid=""):
    df['DI'].to_csv(f'{p_uuid}_DI.csv', index=False, header=False)
    df['DII'].to_csv(f'{p_uuid}_DII.csv', index=False, header=False)
    df['DIII'].to_csv(f'{p_uuid}_DIII.csv', index=False, header=False)
    df['aVR'].to_csv(f'{p_uuid}_aVR.csv', index=False, header=False)
    df['aVL'].to_csv(f'{p_uuid}_aVL.csv', index=False, header=False)
    df['aVF'].to_csv(f'{p_uuid}_aVF.csv', index=False, header=False)
    df['V1'].to_csv(f'{p_uuid}_V1.csv', index=False, header=False)
    df['V2'].to_csv(f'{p_uuid}_V2.csv', index=False, header=False)
    df['V3'].to_csv(f'{p_uuid}_V3.csv', index=False, header=False)
    df['V4'].to_csv(f'{p_uuid}_V4.csv', index=False, header=False)
    df['V5'].to_csv(f'{p_uuid}_V5.csv', index=False, header=False)
    df['V6'].to_csv(f'{p_uuid}_V6.csv', index=False, header=False)

def save_ecg_leads_images(df, p_uuid=""):
    plt.figure(figsize=(10, 1))
    plt.plot(df['DI'])
    plt.axis('off')
    plt.savefig(f'{p_uuid}_DI.png', bbox_inches='tight')

    plt.figure(figsize=(10, 1))
    plt.plot(df['DII'])
    plt.axis('off')
    plt.savefig(f'{p_uuid}_DII.png', bbox_inches='tight')
    
    plt.figure(figsize=(10, 1))
    plt.plot(df['DIII'])
    plt.axis('off')
    plt.savefig(f'{p_uuid}_DIII.png', bbox_inches='tight')

    plt.figure(figsize=(10, 1))
    plt.plot(df['aVR'])
    plt.axis('off')
    plt.savefig(f'{p_uuid}_aVR.png', bbox_inches='tight')

    plt.figure(figsize=(10, 1))
    plt.plot(df['aVL'])
    plt.axis('off')
    plt.savefig(f'{p_uuid}_aVL.png', bbox_inches='tight')

    plt.figure(figsize=(10, 1))
    plt.plot(df['aVF'])
    plt.axis('off')
    plt.savefig(f'{p_uuid}_aVF.png', bbox_inches='tight')

    plt.figure(figsize=(10, 1))
    plt.plot(df['V1'])
    plt.axis('off')
    plt.savefig(f'{p_uuid}_V1.png', bbox_inches='tight')

    plt.figure(figsize=(10, 1))
    plt.plot(df['V2'])
    plt.axis('off')
    plt.savefig(f'{p_uuid}_V2.png', bbox_inches='tight')

    plt.figure(figsize=(10, 1))
    plt.plot(df['V3'])
    plt.axis('off')
    plt.savefig(f'{p_uuid}_V3.png', bbox_inches='tight')

    plt.figure(figsize=(10, 1))
    plt.plot(df['V4'])
    plt.axis('off')
    plt.savefig(f'{p_uuid}_V4.png', bbox_inches='tight')

    plt.figure(figsize=(10, 1))
    plt.plot(df['V5'])
    plt.axis('off')
    plt.savefig(f'{p_uuid}_V5.png', bbox_inches='tight')

    plt.figure(figsize=(10, 1))
    plt.plot(df['V6'])
    plt.axis('off')
    plt.savefig(f'{p_uuid}_V6.png', bbox_inches='tight')

def show_ecg_leads(df):
    fig, axs = plt.subplots(4,3, figsize=(20, 10))

    plt.subplots_adjust(left=0.1, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.4)

    axs[0,0].plot(df['DI'])
    axs[0,0].title.set_text("DI")

    axs[0,1].plot(df['DII'])
    axs[0,1].title.set_text("DII")

    axs[0,2].plot(df['DIII'])
    axs[0,2].title.set_text("DIII")

    axs[1,0].plot(df['aVR'])
    axs[1,0].title.set_text("aVR")

    axs[1,1].plot(df['aVL'])
    axs[1,1].title.set_text("aVL")

    axs[1,2].plot(df['aVF'])
    axs[1,2].title.set_text("aVF")

    axs[2,0].plot(df['V1'])
    axs[2,0].title.set_text("V1")

    axs[2,1].plot(df['V2'])
    axs[2,1].title.set_text("V2")

    axs[2,2].plot(df['V3'])
    axs[2,2].title.set_text("V3")

    axs[3,0].plot(df['V4'])
    axs[3,0].title.set_text("V4")

    axs[3,1].plot(df['V5'])
    axs[3,1].title.set_text("V5")

    axs[3,2].plot(df['V6'])
    axs[3,2].title.set_text("V6")

    plt.show()

def get_amplitude_1mv():
    return 200

def to_mv(signal):
    return signal / get_amplitude_1mv() 

if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='FWC2CSV', description = "FWC to CSV converter - Generate a .CSV file per ECG lead.") 
    parser.add_argument("-i", "--input", help = "FWC input file", required=True)
    parser.add_argument("-u", "--uuid", help = "Patient's UUID", required=True)
    parser.add_argument("-d", "--debug", help = "If the program should display a chat before it finishes.", required=False, action='store_true')
    parser.add_argument("-p", "--pictures", help = "If the program should save PNG images.", required=False, action='store_true')
    args = parser.parse_args()

    f = open(args.input, mode="rb")

    signal = np.zeros((1, 26400))

    data = f.read() 
    samples_offset = data.find(b'\x20\x67\x00\x00') + 4

    samples = data[samples_offset:52800]

    i = 0
    j = 0
    while i <= 52800 - 2:
        bs = samples[i:i + 2]
        sample = int.from_bytes(bs, byteorder='little', signed=True)
        signal[0, j] = sample
        i = i + 2
        j = j + 1

    signal = to_mv(signal)

    lead_series = {
        'DI':   signal[0, 0:2200],
        'DII':  signal[0, 2200:4400],
        'DIII': signal[0, 4400:6600],
        'aVR':  signal[0, 6600:8800],
        'aVL':  signal[0, 8800:11000],
        'aVF':  signal[0, 11000:13200],
        'V1':   signal[0, 13200:15400],
        'V2':   signal[0, 15400:17600],
        'V3':   signal[0, 17600:19800],
        'V4':   signal[0, 19800:22000],
        'V5':   signal[0, 22000:24200],
        'V6':   signal[0, 24200:26400],
    }

    df_leads = pd.DataFrame(data=lead_series)

    if (args.debug):
        show_ecg_leads(df_leads)

    if (args.pictures):
        save_ecg_leads_images(df_leads, p_uuid=args.uuid.upper())

    save_ecg_leads_csv(df_leads, p_uuid=args.uuid.upper()) 
    
    f.close()