import fdb
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

#connection_string = 'C://Users//davyd//OneDrive//Desktop//Coletas//05_02_2024//904397_WINCARDIO.GDB'
#connection_string = 'C://Users//davyd//OneDrive//Desktop//Coletas//05_02_2024//903413_WINCARDIO.GDB'
connection_string = 'C://Users//davyd//development//wincardio//WINCARDIO.GDB'

def connect(database, user="sysdba", password="masterkey"):
    con = fdb.connect(
        host='localhost', database=database,
        user='sysdba', password='masterkey'
    )
    return con


def get_amplitude_1mv():
    return 200


def to_mv(signal):
    return signal / get_amplitude_1mv() 


def blob_to_df(blobReader):
    signal = np.zeros((1, 52800))

    i = 1
    while True:
        bs = blobReader.read(2)

        if (bs == b''):
            break
        sample = int.from_bytes(bs, byteorder='little', signed=True)
        signal[0, i - 1] = sample
        i = i + 1
        
    blobReader.close()

    signal = to_mv(signal)

    lead_series = {
        'DI':   signal[0, 0:4400],
        'DII':  signal[0, 4400:8800],
        'DIII': signal[0, 8800:13200],
        'aVR':  signal[0, 13200:17600],
        'aVL':  signal[0, 17600:22000],
        'aVF':  signal[0, 22000:26400],
        'V1':   signal[0, 26400:30800],
        'V2':   signal[0, 30800:35200],
        'V3':   signal[0, 35200:39600],
        'V4':   signal[0, 39600:44000],
        'V5':   signal[0, 44000:48400],
        'V6':   signal[0, 48400:52800],
    }

    return pd.DataFrame(data=lead_series)

def blob_to_df_2(bvalues):
    signal = np.zeros((1, 52800))

    print(len(bvalues))

    i = 0
    j = 0
    while i <= 52800 - 2:
        bs = bvalues[i:i+2]
        sample = int.from_bytes(bs, byteorder='little', signed=True)
        signal[0, j] = sample
        i = i + 2
        j = j + 1
        
    # blobReader.close()

    signal = to_mv(signal)

    lead_series = {
        'DI':   signal[0, 0:2200],
        'DII':  signal[0, 2200:4400],
        'DIII': signal[0, 4400:6600],
        'aVR':  signal[0, 6600:8800],
        'aVL':  signal[0, 8800:11000],
        'aVF':  signal[0, 11000:13200],
        'V1':   signal[0, 13200:15400],
        'V2':   signal[0, 15400:17600],
        'V3':   signal[0, 17600:19800],
        'V4':   signal[0, 19800:22000],
        'V5':   signal[0, 22000:24200],
        'V6':   signal[0, 24200:26400],
    }
    # print(signal[0])

    return pd.DataFrame(data=lead_series)
    # return pd.DataFrame(data=signal[0])
    

def fetch_patient_record_by_cpf(cpf):
    con = connect(database=connection_string)

    cur = con.cursor()

    cur.execute(f"SELECT ID FROM PACIENTES p WHERE CPF = '{cpf}'")
    result = cur.fetchone()
    patient_id = result[0]    

    cur.execute(f"SELECT ID FROM EXAMES WHERE IDPACIENTE={patient_id} ORDER BY 'DATA' DESC")
    result = cur.fetchone()
    exam_id = result[0] 

    cur.execute(f"SELECT VETORAMOSTRAS FROM REGISTROS WHERE IDEXAME={exam_id} ORDER BY REGISTRONUMERO DESC")
    blobReader = cur.fetchone()[0]

    df = blob_to_df(blobReader)
    
    con.close()

    return df

def fetch_first_patient_record():
    # con = connect(database='C://Users//davyd//development//wincardio//WINCARDIO.GDB')
    con = connect(database='C://Program Files (x86)//Micromed//WinCardio//DB//904397_WINCARDIO.GDB')

    cur = con.cursor()
    cur.execute("select VETORAMOSTRAS from REGISTROS")
    blobReader = cur.fetchone()[0]

    df = blob_to_df(blobReader)
    con.close()

    return df

def fetch_by_registro_id(rid):    
    con = connect(database=connection_string)
    
    cur = con.cursor()
    cur.execute(f'select VETORAMOSTRAS from REGISTROS WHERE ID={rid}')
    bvalues = cur.fetchone()[0]
    # print(type(blobReader))
    # print(dir(blobReader))

    if "903413" in connection_string:
        df = blob_to_df_2(bvalues)
    else:
        df = blob_to_df(bvalues)

    con.close()

    return df


def show_ecg_leads(df):
    fig, axs = plt.subplots(4,3, figsize=(20, 10))

    plt.subplots_adjust(left=0.1, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.4)

    axs[0,0].plot(df['DI'])
    axs[0,0].title.set_text("DI")

    axs[0,1].plot(df['DII'])
    axs[0,1].title.set_text("DII")

    axs[0,2].plot(df['DIII'])
    axs[0,2].title.set_text("DIII")

    axs[1,0].plot(df['aVR'])
    axs[1,0].title.set_text("aVR")

    axs[1,1].plot(df['aVL'])
    axs[1,1].title.set_text("aVL")

    axs[1,2].plot(df['aVF'])
    axs[1,2].title.set_text("aVF")

    axs[2,0].plot(df['V1'])
    axs[2,0].title.set_text("V1")

    axs[2,1].plot(df['V2'])
    axs[2,1].title.set_text("V2")

    axs[2,2].plot(df['V3'])
    axs[2,2].title.set_text("V3")

    axs[3,0].plot(df['V4'])
    axs[3,0].title.set_text("V4")

    axs[3,1].plot(df['V5'])
    axs[3,1].title.set_text("V5")

    axs[3,2].plot(df['V6'])
    axs[3,2].title.set_text("V6")

    plt.show()

def save_ecg_leads_csv(df, rid=""):
    df['DI'].to_csv(f'{rid}_DI.csv', index=False, header=False)
    df['DII'].to_csv(f'{rid}_DII.csv', index=False, header=False)
    df['DIII'].to_csv(f'{rid}_DIII.csv', index=False, header=False)
    df['aVR'].to_csv(f'{rid}_aVR.csv', index=False, header=False)
    df['aVL'].to_csv(f'{rid}_aVL.csv', index=False, header=False)
    df['aVF'].to_csv(f'{rid}_aVF.csv', index=False, header=False)
    df['V1'].to_csv(f'{rid}_V1.csv', index=False, header=False)
    df['V2'].to_csv(f'{rid}_V2.csv', index=False, header=False)
    df['V3'].to_csv(f'{rid}_V3.csv', index=False, header=False)
    df['V4'].to_csv(f'{rid}_V4.csv', index=False, header=False)
    df['V5'].to_csv(f'{rid}_V5.csv', index=False, header=False)
    df['V6'].to_csv(f'{rid}_V6.csv', index=False, header=False)

def save_ecg_leads_images(df, rid=""):    
    plt.figure(figsize=(10, 1))
    plt.plot(df['DI'])
    plt.axis('off')
    plt.savefig(f'{rid}_DI.png', bbox_inches='tight')

    plt.figure(figsize=(10, 1))
    plt.plot(df['DII'])
    plt.axis('off')
    plt.savefig(f'{rid}_DII.png', bbox_inches='tight')
    
    plt.figure(figsize=(10, 1))
    plt.plot(df['DIII'])
    plt.axis('off')
    plt.savefig(f'{rid}_DIII.png', bbox_inches='tight')

    plt.figure(figsize=(10, 1))
    plt.plot(df['aVR'])
    plt.axis('off')
    plt.savefig(f'{rid}_aVR.png', bbox_inches='tight')

    plt.figure(figsize=(10, 1))
    plt.plot(df['aVL'])
    plt.axis('off')
    plt.savefig(f'{rid}_aVL.png', bbox_inches='tight')

    plt.figure(figsize=(10, 1))
    plt.plot(df['aVF'])
    plt.axis('off')
    plt.savefig(f'{rid}_aVF.png', bbox_inches='tight')

    plt.figure(figsize=(10, 1))
    plt.plot(df['V1'])
    plt.axis('off')
    plt.savefig(f'{rid}_V1.png', bbox_inches='tight')

    plt.figure(figsize=(10, 1))
    plt.plot(df['V2'])
    plt.axis('off')
    plt.savefig(f'{rid}_V2.png', bbox_inches='tight')

    plt.figure(figsize=(10, 1))
    plt.plot(df['V3'])
    plt.axis('off')
    plt.savefig(f'{rid}_V3.png', bbox_inches='tight')

    plt.figure(figsize=(10, 1))
    plt.plot(df['V4'])
    plt.axis('off')
    plt.savefig(f'{rid}_V4.png', bbox_inches='tight')

    plt.figure(figsize=(10, 1))
    plt.plot(df['V5'])
    plt.axis('off')
    plt.savefig(f'{rid}_V5.png', bbox_inches='tight')

    plt.figure(figsize=(10, 1))
    plt.plot(df['V6'])
    plt.axis('off')
    plt.savefig(f'{rid}_V6.png', bbox_inches='tight')

if __name__ == "__main__":
    # df = fetch_first_patient_record()
    df = fetch_patient_record_by_cpf("02698258357")
    
    #df = fetch_by_registro_id(31354) # AILA MARIA DE SOUZA RODRIGUES
    #df = fetch_by_registro_id(31475) # MAURO BARBOSA DE CASTRO

    #df = fetch_by_registro_id(86055) # MANOEL PINHEIRO DA SILVA
    #df = fetch_by_registro_id(86087) # RAIMUNDA RENILDA NOGUEIRA XAVIER

    show_ecg_leads(df)
    #save_ecg_leads_images(df, 31475)
    #save_ecg_leads_csv(df, 31475)

    # df = fetch_first_patient_record()
    # show_ecg_leads(df)

   













